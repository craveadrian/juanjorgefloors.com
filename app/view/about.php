<div id="content">
	<div class="row">
		<h1>About Us</h1>
		<div class="about-us-content">
			<h2>JUAN JORGE FLOORS </h2>
			<p>You can’t lose- better flooring for less money <br>Giving your commercial and residential the right feel. Take advantage of our great pricing on durable and elegant flooring, 9 years member of the Better Business Bureau.</p>
			<p>For a room to look its best, it needs to have the right kind of floor. Though easy to overlook, a matching, quality flooring is necessary for creating satisfying decor. This is one reason why wood flooring is such an important part of design.</p>
			<p>Many kinds of residential and commercial spaces call for a quality installation of hardwood flooring. You can't go wrong with Juan Jorge Floors, Inc. helping you with any new hardwood floor installation or LVT flooring. If you are restoring an older home or business, we offer hardwood floor refinishing to give the original wood a new sheen.</p>
			<p>We provide a number of flooring services from installation and repair to a complete wood floor replacement. If you are looking for a reliable flooring contractor in Portsmouth, VA, we are at your service. Call Juan Jorge Floors, Inc. today to learn more about what we do. Our flooring company would be happy to discuss all of our options with you.</p>
		</div>
	</div>
</div>
