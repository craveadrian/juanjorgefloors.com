<div id="content1">
	<div class="row">
		<div id="c1-left">
			<h3>NEED A PROFESSIONAL <span>FLOORING CONTRACTOR IN</span></h3>
			<h2>PORTSMOUTH, VA</h2>
		</div>

		<div id="c1-right">
			<dl>
				<dt><img src="public/images/common/c1-img1.png"></dt>
				<span class="clearfix"></span>
				<dd>
				<b>DEDICATED TO CUSTOMER SATISFACTION</b>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.</p></dd>
			</dl>

			<dl>
				<dt><img src="public/images/common/c1-img2.png"></dt>
				<span class="clearfix"></span>
				<dd>
				<b>WIDE SELECTION OF QUALITY SERVICES</b>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.</p></dd>
			</dl>

			<dl>
				<dt><img src="public/images/common/c1-img3.png"></dt>
				<span class="clearfix"></span>
				<dd>
				<b>AFFORDABLE PRICES</b>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.</p></dd>
			</dl>
		</div>


	</div>
</div>


<div id="content2">
	<div class="row">
		<h3 class="titles">OUR SERVICES</h3>
		<p>We provide a number of flooring services from installation and repair to a complete wood floor replacement. If you are looking for a reliable flooring contractor in Portsmouth, VA, we are at your service. Call Juan Jorge Floors, Inc. today to learn more about what we do. Our flooring company would be happy to discuss all of our options with you.</p>

		<dl class="style1">
			<dt><img src="public/images/common/c2-img1.jpg"></dt>
			<dd>
				<h3>HARWOOD FLOORING SERVICES</h3>
				<p>Juan Jorge Floors, Inc. is the expert in the flooring department and provides a wide selection of wood flooring services. Maybe you want to change out the stained old carpet, or maybe you are tired of the laminate being easily marked up and scratched. Whatever the case may be, our experienced team is up to wood floor repair, installation, refinishing, and many other services.</p>

				<a href="services#harwood-flooring-services"><button class="btn-blue">LEARN MORE</button></a>
			</dd>
		</dl>

		<dl class="style2">
			<dt>
				<h3>LVT FLOORING</h3>
				<p>Laminate flooring provides a convenient modern alternative to hardwood flooring. Our laminate flooring installation service uses multiple layers of resin and other substances to create something that looks exactly like genuine wood.</p>

				<a href="services#lvt-flooring"><button class="btn-blue">LEARN MORE</button></a>
			</dt>

			<dd><img src="public/images/common/c2-img2.jpg"></dd>
		</dl>

		<dl class="style1">
			<dt><img src="public/images/common/c2-img3.jpg"></dt>
			<dd>
				<h3>INSTALLATION FLOORING​</h3>
				<p>Juan Jorge Floors, Inc. is the number one expert in home flooring and lamination services. We offer a wide selection of flooring options and wood supplies. If you want to give your home an upgraded look, let our flooring contractor provide you with durable hardwood flooring.</p>

				<a href="services#installation-flooring"><button class="btn-blue">LEARN MORE</button></a>
			</dd>
		</dl>

	</div>
</div>

<div id="content3">
	<div class="row">
		<div id="c3-left">
			<h3 class="titles">FLOORING CONTRACTOR</h3>
			<h4>IN PORTSMOUTH, VA</h4>
			<p>For a room to look its best, it needs to have the right kind of floor. Though easy to overlook, a matching, quality flooring is necessary for creating satisfying decor. This is one reason why wood flooring is such an important part of design.
			<br> <br>
			Many kinds of residential and commercial spaces call for a quality installation of hardwood flooring. You can’t go wrong with Juan Jorge Floors, Inc. helping you with any new hardwood floor installation or LVT flooring. If you are  restoring an older home or business, we offer hardwood floor refinishing to give the original wood a new sheen.</p>
		</div>

		<img src="public/images/common/c3-logo.png">

	</div>
</div>

<div id="content4">
	<div class="row">
		<h3 class="titles-black">OUR LATEST PROJECTS</h3>
		<div id="c4-img-row">
			<img src="public/images/common/c4-img1.jpg">
			<img src="public/images/common/c4-img2.jpg">
			<img src="public/images/common/c4-img3.jpg">
			<img src="public/images/common/c4-img4.jpg">
			<img src="public/images/common/c4-img5.jpg">
			<img src="public/images/common/c4-img6.jpg">
		</div>

		<img src="public/images/common/c4-img7.jpg" id="c4-img7">

		<a href="gallery#content"><button class="btn-blue">VIEW MORE</button></a>

	</div>
</div>


<div id="content5">
	<div class="row">
		<h3 class="titles-black">WHY CHOOSE US?</h3>

		<div id="c5-content">
			<dl>
				<dt><img src="public/images/common/c5-img1.png"></dt>
				<dd>
					<h4>DEDICATED TO CUSTOMER SATISFACTION</h4>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
				</dd>
			</dl>

			<dl>
				<dt><img src="public/images/common/c5-img2.png"></dt>
				<dd>
					<h4>WIDE SELECTION OF QUALITY SERVICES</h4>
					<p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
				</dd>
			</dl>

			<dl>
				<dt><img src="public/images/common/c5-img3.png"></dt>
				<dd>
					<h4>AFFORDABLE PRICES</h4>
					<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros.</p>
				</dd>
			</dl>
		</div>

	</div>

</div>
