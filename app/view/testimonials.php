<div id="content">
	<div class="row">
		<h1>Testimonials</h1>
		<h2>REVIEWS</h2>
			<a href="https://en.yelp.com.ph/biz/juan-jorge-floors-portsmouth?hrid=CCKm1F7C-7fV6uO5zJ0_4Q" rel="nofollow noopener" target="_blank">
				<div class="reviews">
					<h3> <span>	&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 7/3/2018</h3>
					<p class="rvw-comment">This was the best experience I ever had with contractor. Juan and his team did a awsome job There went above and beyond .So professional Awesome price.  Can't wait to get my last room done and the rental property.  There were a few pieces of furniture very heavy I couldn't get out of the smalls rooms .There managed All I can say I love love love my new floors .thank you It is hard these days to find someone who is honest and u feel comfortable working in ur house . This was the right team ..All I can say hire them and u be happy in ur house and wallet. </p>
					<h4>Elke H. | Spartan Village, Norfolk, United States</h4>
			</div>
			</a>
			<a href="https://en.yelp.com.ph/biz/juan-jorge-floors-portsmouth?hrid=NKeIL2sn0o2UMrzbQkGkMw" rel="nofollow noopener" target="_blank">
				<div class="reviews">
					<h3> <span>	&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 4/23/2018</h3>
					<p class="rvw-comment">Usually you have to choose between value, quality and speed. Not with Juan! He delivered all three, and I could not be more pleased. He patiently addressed all of my concerns, and stayed late while I decided on the perfect floor color and finish.He is very professional, polite and easy to communicate with. I have recommended him to all my friends and colleagues and will continue to do so! </p>
					<h4>Kirsty G. | Norfolk, United States</h4>
				</div>
			</a>
			<a href="https://en.yelp.com.ph/biz/juan-jorge-floors-portsmouth?hrid=oX23jj8ZDQ6Dh2VrrYAoiA" rel="nofollow noopener" target="_blank">
				<div class="reviews">
					<h3> <span>	&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 6/29/2017</h3>
					<p class="rvw-comment">Juan did an incredible job for us! From estimate through completing the job, he was wonderful. </p>
					<p class="rvw-comment">He refinished about 1,300 sq ft of original pine floors in our Ghent home. They had DEFINITELY seen better days (can you say lime green paint?) but you can't even tell now. The best part? He started earlier than he had estimated and finished in THREE DAYS. He and his crew worked their butts off working long hours to finish the job, and dang did they do well. Also hats off to his crew, they were courteous and nice. </p>
					<p class="rvw-comment">He worked around our movers schedule too, and when they broke a window (ugh) he even offered to tape the window for us. </p>
					<p class="rvw-comment">His knowledge of wood and its properties is incredible, and his pricing just couldn't be beat. He deserves your business!!!</p>
					<h4>Eva H. | Norfolk, United States</h4>
				</div>
			</a>
			<a href="https://en.yelp.com.ph/biz/juan-jorge-floors-portsmouth?hrid=n-1y4DVwmEBbYK_KhHjuPQ" rel="nofollow noopener" target="_blank">
				<div class="reviews">
					<h3> <span>	&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 1/23/2016</h3>
					<p class="rvw-comment">Juan recently refinished my bamboo floors from a very dark finish to just a shade darker than natural and I couldn't be happier with them.  He and his crew were prompt, friendly, courteous and reasonably priced. Juan took the time to explain every step and answer all my questions before, during and after the work was done. </p>
					<p class="rvw-comment">The floors look amazing!</p>
					<p class="rvw-comment">I would definitely hire Juan and his crew to do work for me again.</p>
					<h4>Nancy H. | Chesapeake, United States</h4>
				</div>
			</a>
			<a href="https://en.yelp.com.ph/biz/juan-jorge-floors-portsmouth?hrid=_no-fVy2ekD_juNWJnIk-w" rel="nofollow noopener" target="_blank">
				<div class="reviews">
					<h3> <span>	&#9733; &#9733; &#9733; &#9733; &#9733;</span> | 8/26/2016</h3>
					<p class="rvw-comment">I've had floors put in couple houses, and aside from my floors, I've seen a lot of Juan's work. I don't know that there is a more skilled hardwood flooring expert. The quality and attention to detail are the most important parts and Juan does not compromise.</p>
					<h4>Sean H. | Beaverton, United States</h4>
				</div>
			</a>
			<a href="https://en.yelp.com.ph/biz/juan-jorge-floors-portsmouth" target="_blank" class="btn">READ MORE</a>
	</div>
</div>
