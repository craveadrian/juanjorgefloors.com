<div id="content">
	<div class="row">
		<h1>Services</h1>
		<div id="harwood-flooring-services">
			<h2>Harwood Flooring Services</h2>
			<div class="left">
				<img src="public/images/common/c2-img1.jpg">
			</div>
			<div class="right">
				<p>Juan Jorge Floors, Inc. is the expert in the flooring department and provides a wide selection of wood flooring services. Maybe you want to change out the stained old carpet, or maybe you are tired of the laminate being easily marked up and scratched. Whatever the case may be, our experienced team is up to wood floor repair, installation, refinishing, and many other services.</p>
				<p>The choices may seem endless when it comes to deciding materials, layout, colors, and patterns, but our flooring contractor has a lot of knowledge of what looks good and fits with your space. You definitely want a professional when it comes to wood floor installation or repair.</p>
				<p>Juan Jorge Floors, Inc. is known to give exceptional hardwood flooring services. We have the proper equipment to make sure your floors are pristine. We have the best pricing, beat any estimate, and have a hand picked expert staff. We follow research about new products, colors and floor finish trends. If you want new hardwood floors in your property or need repairs, we can help.</p>
				<p>Contact Juan Jorge Floors, Inc. to get the best commercial and residential hardwood flooring services in Portsmouth, VA.</p>
			</div>
		</div>
		<div id="lvt-flooring">
			<h2>LVT Flooring</h2>
			<div class="left">
				<p>Laminate flooring provides a convenient modern alternative to hardwood flooring. Our laminate flooring installation service uses multiple layers of resin and other substances to create something that looks exactly like genuine wood.</p>
				<p>Because laminate flooring is easy and affordable to install, and delivers such pleasant results, many people are taking this choice for their own flooring needs. Not only that, but laminate floors are very low-maintenance compared to traditional engineered hardwood flooring. As they are made from a more sleek surface, accidental spills are less damaging to laminate floors than wood flooring. Laminate is ideal for offices and budget apartments. Though it will wear out after a while, our remodeling contractors will be able to replace the laminate floor quickly and without much trouble. </p>
				<p>If you are unsure about the laminate floor benefits you can always contact our knowledgeable laminate flooring contractor and he or she will be happy to answer any questions or concerns you have. To learn more about our flooring services, get in touch with Juan Jorge Floors, Inc. , located in Portsmouth, VA.</p>
			</div>
			<div class="right">
				<img src="public/images/common/c2-img2.jpg">
			</div>
		</div>
		<div id="installation-flooring">
			<h2>Installation Flooring​</h2>
			<div class="left">
				<img src="public/images/common/c2-img3.jpg">
			</div>
			<div class="right">
				<p>Juan Jorge Floors, Inc. is the number one expert in home flooring and lamination services. We offer a wide selection of flooring options and wood supplies. If you want to give your home an upgraded look, let our flooring contractor provide you with durable hardwood flooring.</p>
				<p>Our supply of floor colors, materials, and patterns are available to you to see what will best suit your home. Nobody beats our service for hardwood floor installation. We guarantee quick installment without compromising excellence. Our hardwood floors will last for decades and will give you the durability and reliability that we our company promises.</p>
				<p>At Juan Jorge Floors, Inc. , we are known to give exceptional hardwood flooring services. We have the proper equipment to get your floors to shine in the light. We have the best pricing, beat any estimate, and have a hand picked expert staff. We follow research about new products, colors and floor finish trends. If you want new hardwood floors in your property or need repairs, we can help.</p>
				<p>We are known for our exceptional wood floor services. If you want new wood floors or have questions regarding general contracting, we can help. Contact Juan Jorge Floors, Inc. today in Portsmouth, VA for the best floors around!</p>
			</div>
		</div>
	</div>
</div>
